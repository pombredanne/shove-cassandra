#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''setup for shove'''

from os import getcwd
from os.path import join

from setuptools import setup, find_packages


def getversion(fname):
    '''Get the __version__ without importing.'''
    with open(fname) as f:
        for line in f:
            if line.startswith('__version__'):
                return '%s.%s.%s' % eval(line[13:].rstrip())

install_requires = list(l.strip() for l in open(
    join(getcwd(), 'reqs/requires.txt'),
))

setup(
    name='shove',
    version=getversion('shove/__init__.py'),
    description='Common object storage frontend',
    long_description=open('README.rst').read(),
    author='L. C. Rees',
    author_email='lcrees@gmail.com',
    url='https://bitbucket.org/lcrees/shove/',
    license='BSD',
    packages=find_packages(),
    test_suite='shove.tests',
    install_requires=install_requires,
    zip_safe=False,
    keywords='object storage persistence database shelve',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Database :: Front-Ends',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
    ],
    entry_points='''
    [shove.stores]
    bsddb=shove_bsdb.bsdb:BSDBStore
    cassandra=shove_cassandra.store:CassandraStore
    dbm=shove.store:DBMStore
    durus=shove_durus.durusdb:DurusStore
    file=shove.store:FileStore
    firebird=shove_sqlalchemy.db:DBStore
    ftp=shove_ftp.ftp:FTPStore
    hdf5=shove_hdf5.hdf5:HDF5Store
    hstore=shove_hstore.hstore:HStore
    leveldb=shove_leveldb.leveldbstore:LevelDBStore
    memory=shove.store:MemoryStore
    mongodb=shove_mongodb.mongodb:MongoDBStore
    mssql=shove_sqlalchemy.db:DBStore
    mysql=shove_sqlalchemy.db:DBStore
    oracle=shove_sqlalchemy.db:DBStore
    postgres=shove_sqlalchemy.db:DBStore
    redis=shove_redis.redisdb:RedisStore
    s3=shove_s3.s3:S3Store
    simple=shove.store:SimpleStore
    sqlite=shove_sqlalchemy.db:DBStore
    zodb=tests.zodb:ZODBStore
    hg=shove_hg.hgstore:HgStore
    git=shove_git.gitstore:GitStore
    [shove.caches]
    file=shove.cache:FileCache
    filelru=shove.cache:FileLRUCache
    firebird=shove_sqlalchemy.cache.db:DBCache
    memcache=shove_memcache.memcached:MemCache
    memlru=shove.cache:MemoryLRUCache
    memory=shove.cache:MemoryCache
    mssql=shove_sqlalchemy.cache.db:DBCache
    mysql=shove_sqlalchemy.cache.db:DBCache
    oracle=shove_sqlalchemy.cache.db:DBCache
    postgres=shove_sqlalchemy.cache.db:DBCache
    redis=shove_redis.redisdb:RedisCache
    simple=shove.cache:SimpleCache
    simplelru=shove.cache:SimpleLRUCache
    sqlite=shove_sqlalchemy.cache.db:DBCache
    ''',
)
