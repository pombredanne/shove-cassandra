from stuf.six import PY3, unittest
from tests.mixins import Spawn
from tests.test_store import Store


if not PY3:

    @unittest.skip('reason')
    class TestBSDBStore(Store, unittest.TestCase):

        initstring = 'bsddb://test.db'

        def tearDown(self):
            import os
            self.store.close()
            os.remove('test.db')

    class TestCassandraStore(Store, Spawn, unittest.TestCase):

        cmd = ['cassandra', '-f']

        @classmethod
        def setUpClass(cls):
            super(TestCassandraStore, cls).setUpClass()

        def setUp(self):
            from shove import Shove
#            from pycassa.system_manager import SystemManager  # @UnresolvedImport @IgnorePep8
#            system_manager = SystemManager('localhost:9160')
#            system_manager.create_column_family('Murk', 'shove')
            self.store = Shove('cassandra://localhost:9160/Murk/shove')

        def tearDown(self):
            if self.store._store is not None:
                self.store.clear()
                self.store.close()
            from pycassa.system_manager import SystemManager  # @UnresolvedImport @IgnorePep8
            system_manager = SystemManager('localhost:9160')
            system_manager.drop_column_family('Murk', 'shove')

        @classmethod
        def tearDownClass(cls):
            import time
            from fabric.api import local
            local('killall java')
            time.sleep(15.0)